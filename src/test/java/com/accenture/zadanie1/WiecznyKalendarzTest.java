package com.accenture.zadanie1;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static junit.framework.TestCase.assertEquals;

public class WiecznyKalendarzTest {

    @Test
    public void dzienLadowaniaNaKsiezycuToNiedziela() {
        assertEquals(LocalDate.of(1969, Month.JULY, 20).getDayOfWeek().toString(), WiecznyKalendarz.jakiToDzien(LocalDate.of(1969, Month.JULY, 20)).toString());
    }

    @Test
    public void sklodowskaCurrieUrodzilaSieWCzwartek() {
        assertEquals(LocalDate.of(1867,11,7).getDayOfWeek().toString(), WiecznyKalendarz.jakiToDzien(LocalDate.of(1867,11,7)).toString());
    }

    @Test
    public void einsteinUrodzilSieWPiatek() {
        assertEquals(LocalDate.of(1879, Month.MARCH, 14).getDayOfWeek().toString(),WiecznyKalendarz.jakiToDzien(LocalDate.of(1879, Month.MARCH, 14)).toString());
    }

    @Test
    public void w2018SylwesterWypadaWPoniedzialek() {
        assertEquals(LocalDate.of(2018, Month.DECEMBER, 31).getDayOfWeek().toString(), WiecznyKalendarz.jakiToDzien(LocalDate.of(2018, Month.DECEMBER, 31)).toString());
    }

    @Test
    public void dzienPremieryNowejNadzieiToSroda() {
        assertEquals(LocalDate.of(1977, Month.MAY, 25).getDayOfWeek().toString(),WiecznyKalendarz.jakiToDzien(LocalDate.of(1977, Month.MAY, 25)).toString());
    }

    @Test
    public void dzienWstapieniePolskiDoUEToSobota() {
        assertEquals(LocalDate.of(2004, Month.MAY, 1).getDayOfWeek().toString(),WiecznyKalendarz.jakiToDzien(LocalDate.of(2004, Month.MAY, 1)).toString());
    }

    @Test
    public void wigiliaW2050WypadaWSobote() {
        assertEquals(LocalDate.of(2050, Month.DECEMBER, 24).getDayOfWeek().toString(),WiecznyKalendarz.jakiToDzien(LocalDate.of(2050, Month.DECEMBER, 24)).toString());
    }
}
