package com.accenture.zadanie2;

import org.junit.Test;


import static com.accenture.zadanie2.PalindromRekurencja.czyPalindrom;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertTrue;


public class PalindromRekurencjaTest {

    @Test
    public void nullZwracaNull() {
        assertNull(czyPalindrom(null));
    }

    @Test
    public void pustyLancuchZwracaNull() {
        assertNull(czyPalindrom(""));
    }

    @Test
    public void kajakJestPalindromem() {
        assertTrue(czyPalindrom("kajak"));
    }

    @Test
    public void zaradnydyndarazJestPalindromem() {
        assertTrue(czyPalindrom("zaradnydyndaraz"));
    }

    @Test
    public void comidalduchcudladimocJestPalindromem() {
        assertTrue(czyPalindrom("comidałduchcudładimoc"));
    }

    @Test
    public void devillivedJestPalindromem() {
        assertTrue(czyPalindrom("devillived"));
    }

    @Test
    public void waniliaNieJestPalindromem() {
        assertFalse(czyPalindrom("wanilia"));
    }

    @Test
    public void javaNieJestPalindromem() {
        assertFalse(czyPalindrom("java"));
    }

}
