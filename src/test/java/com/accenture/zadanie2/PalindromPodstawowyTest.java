package com.accenture.zadanie2;

import org.junit.Test;


import static com.accenture.zadanie2.PalindromPodstawowy.czyPalindrom;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;


public class PalindromPodstawowyTest {

    @Test
    public void nullZwracaNull(){
        assertNull(czyPalindrom(null));
    }
    @Test
    public void pustyLancuchZwracaNull(){
        assertNull(czyPalindrom(""));
    }

    @Test
    public void kajakJestPalindromem(){
        assertTrue(czyPalindrom("kajak"));
    }

    @Test
    public void zaradnydyndarazJestPalindromem(){
        assertTrue(czyPalindrom("zaradnydyndaraz"));
    }

    @Test
    public void comidalduchcudladimocJestPalindromem(){
        assertTrue(czyPalindrom("comidałduchcudładimoc"));
    }

    @Test
    public void devillivedJestPalindromem(){
        assertTrue(czyPalindrom("devillived"));
    }

}

