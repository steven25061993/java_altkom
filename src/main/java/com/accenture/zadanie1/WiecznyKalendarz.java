package com.accenture.zadanie1;

import java.time.LocalDate;

/**
 * Klasa WiecznyKalendarz w ktorej znajduje sie metoda {@link WiecznyKalendarz#jakiToDzien(LocalDate data) pozwalajaca}
 * na wskazaniu dnia tygodnia na podstawie daty
 *
 * @author Damian Futyma
 */
public final class WiecznyKalendarz {


    private WiecznyKalendarz() {
    }

    /**
     * Parametr pomocniczy z, wykorzystywany w algorytmie
     */
    private static int z;
    /**
     * Parametr pomocniczy c, wykorzystywany w algorytmie
     */
    private static int c;

    /**
     * Metoda pozwalalajaca zwrocic nazwe dnia tygodnia na podstawie podanej daty.
     *
     * @param data Dowolna data w formacie {@link LocalDate}
     * @return Dzien tygodnia wybrany z klasy {@link DzienTygodnia}
     */
    public static DzienTygodnia jakiToDzien(LocalDate data) {
        int d = data.getDayOfMonth();
        int m = data.getMonthValue();
        int y = data.getYear();

        ustawianieWartosciPomocniczych(m, y);
        int sygnaturaDnia = algorytm(m, y, d);
        return DzienTygodnia.values()[sygnaturaDnia];
    }

    /**
     * Metoda wewnatrz klasy {@link WiecznyKalendarz#jakiToDzien(LocalDate data)} majaca na celu ustalenie
     * zmiennych z oraz c w algorytmie obliczania dnia tygodnia na podstawie daty
     *
     * @param m reprezentacja miesiaca w dacie podanej przez uzykownika
     * @param y reprezentacja roku w dacie podanej przez uzykownika
     */
    private static void ustawianieWartosciPomocniczych(final int m, final int y) {
        if (m < 3) {
            z = y - 1;
            c = 0;
        } else {
            z = y;
            c = 2;
        }
    }

    /**
     * Metoda wewnatrz klasy {@link WiecznyKalendarz#jakiToDzien(LocalDate data)} zawierajaca algorytm obliczania dnia tyodnia
     * na podstawie daty
     *
     * @param m reprezentacja miesiaca w dacie podanej przez uzykownika
     * @param y reprezentacja roku w dacie podanej przez uzykownika
     * @param d reprezentacja dnia w dacie podanej przez uzykownika
     * @return liczba z zakresu od 0 do 6 odpowiadajaca jednemu z dni tygodnia w klasie {@link DzienTygodnia}
     */
    private static int algorytm(final int m, final int y, final int d) {
        return ((23 * m / 9) + d + 4 + y + (z / 4) - (z / 100) + (z / 400) - c) % 7;
    }
}
