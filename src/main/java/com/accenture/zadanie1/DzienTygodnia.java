package com.accenture.zadanie1;

/**
 * Klasa enum zawierajaca dni tygodnia od niedzieli do poniedzialku. Uzyte sa nazwy angielskie aby mozna bylo przeprowadzic testy.
 *
 * @author Damian Futyma
 */
public enum DzienTygodnia {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
}
