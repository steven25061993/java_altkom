package com.accenture.zadanie4;

/**
 * Klasa testujaca dzialanie mojego buildera poprzez wyswietlanie roznych kombinacji ogloszenia
 *
 * @author Damian Futyma
 */
public class TestSamochodu {
    public static void main(String[] args) {

        Samochod podstawowySamochod = new Samochod.Budowniczy("BMW", "A4", 7500)
                .buduj();
        System.out.println(podstawowySamochod);
        Samochod uzywanySamochod = new Samochod.Budowniczy("BMW", "A4", 7500)
                .stan(Stan.UZYWANY)
                .buduj();
        System.out.println(uzywanySamochod);
        Samochod samochodZ2004 = new Samochod.Budowniczy("BMW", "A4", 7500)
                .stan(Stan.UZYWANY)
                .rokProdukcji(2004)
                .buduj();
        System.out.println(samochodZ2004);
        Samochod samochodZMalymPrzebiegiem = new Samochod.Budowniczy("BMW", "A4", 7500)
                .stan(Stan.UZYWANY)
                .rokProdukcji(2004)
                .przebieg(2000)
                .buduj();
        System.out.println(samochodZMalymPrzebiegiem);
        Samochod samochodElektryczny = new Samochod.Budowniczy("BMW", "A4", 7500)
                .stan(Stan.UZYWANY)
                .rokProdukcji(2004)
                .przebieg(2000)
                .rodzajPaliwa(RodzajPaliwa.ELEKTRYCZNY)
                .buduj();
        System.out.println(samochodElektryczny);
        Samochod samochodOPojemnosci3000cm3 = new Samochod.Budowniczy("BMW", "A4", 7500)
                .stan(Stan.UZYWANY)
                .rokProdukcji(2004)
                .przebieg(2000)
                .rodzajPaliwa(RodzajPaliwa.ELEKTRYCZNY)
                .pojemnoscSilnika(3000)
                .buduj();
        System.out.println(samochodOPojemnosci3000cm3);
    }
}
