package com.accenture.zadanie4;


import java.util.ArrayList;
import java.util.List;

/**
 * Klasa Samochod ktorej celem jest zbudowanie obiektu samochod oraz przygotowanie go do wyswietlenia w postaci ogloszenia
 * sprzedazy samochodu
 */
public class Samochod {
    /**
     * Marka samochodu typu String
     */
    private String marka;
    /**
     * Model samochodu typu String
     */
    private String model;
    /**
     * Stan samochodu z klasy Stan
     */
    private Stan stan;
    /**
     * Cena samochodu typu Integer
     */
    private Integer cena;
    /**
     * Moc silnika typu Integer
     */
    private Integer pojemnoscSilnika;
    /**
     * Rok produkcji samochodu typu Integer
     */
    private Integer rokProdukcji;
    /**
     * Przebieg samochodu typu Integer
     */
    private Integer przebieg;
    /**
     * Rodzaj paliwa jakiego uzywa samochod z klasy RodzajPaliwa
     */
    private RodzajPaliwa rodzajPaliwa;

    private Samochod(final Budowniczy budowniczy) {
        this.marka = budowniczy.marka;
        this.model = budowniczy.model;
        this.stan = budowniczy.stan;
        this.pojemnoscSilnika = budowniczy.pojemnoscSilnika;
        this.cena = budowniczy.cena;
        this.rokProdukcji = budowniczy.rokProdukcji;
        this.przebieg = budowniczy.przebieg;
        this.rodzajPaliwa = budowniczy.rodzajPaliwa;
    }


    public static class Budowniczy {

        private final String marka;
        private final String model;
        private Stan stan;
        private final Integer cena;
        private Integer pojemnoscSilnika;
        private Integer rokProdukcji;
        private Integer przebieg;
        private RodzajPaliwa rodzajPaliwa;

        /**
         * Konstruktor budowniczego, zawiera on wszystkie elementy obowiazkowe przy tworzeniu obiektu samochod.
         * Pozostale elementy ogloszenia sa opcjonalne.
         *
         * @param marka marka samochodu
         * @param model model samochodu
         * @param cena  cena samochodu
         */
        public Budowniczy(final String marka, final String model, final Integer cena) {
            this.marka = marka;
            this.model = model;
            this.cena = cena;
        }

        /**
         * Metoda majaca na celu wprowadzenie parametru stan do obiektu samochod
         *
         * @param stan stan pojazdu z klasy enum
         * @return Obiekt samochodu z dodanym parametrem stan
         */
        public Budowniczy stan(final Stan stan) {
            this.stan = stan;
            return this;
        }

        /**
         * Metoda majaca na celu wprowadzenie parametru rokProdukcji do obiektu samochod
         *
         * @param rokProdukcji rokProdukcji pojazdu z klasy Integer
         * @return Obiekt samochodu z dodanym parametrem rokProdukcji
         */
        public Budowniczy rokProdukcji(final Integer rokProdukcji) {
            this.rokProdukcji = rokProdukcji;
            return this;
        }

        /**
         * Metoda majaca na celu wprowadzenie parametru przebieg do obiektu samochod
         *
         * @param przebieg przebieg pojazdu bedacy liczba calkowita
         * @return Obiekt samochodu z dodanym parametrem przebieg
         */
        public Budowniczy przebieg(final Integer przebieg) {
            this.przebieg = przebieg;
            return this;
        }

        /**
         * Metoda ma na celu wprowadzenie parametru przebieg do obiektu samochod
         *
         * @param pojemnoscSilnika pojemnosc silnika bedaca liczba calkowita
         * @return Obiekt samochodu z dodanym parametrem pojemnosci silnika
         */
        public Budowniczy pojemnoscSilnika(final Integer pojemnoscSilnika) {
            this.pojemnoscSilnika = pojemnoscSilnika;
            return this;
        }

        /**
         * Metoda majaca na celu wprowadzenie parametru rodzajPaliwa do obiektu samochod
         *
         * @param rodzajPaliwa rodzajPaliwa pojazdu z klasy Enum
         * @return Obiekt samochodu z dodanym parametrem rodzajPaliwa
         */
        public Budowniczy rodzajPaliwa(final RodzajPaliwa rodzajPaliwa) {
            this.rodzajPaliwa = rodzajPaliwa;
            return this;
        }

        /**
         * Metoda konczaca budowe obiektu budowniczy. Zwraca ona przygotowany przez nas obiekt.
         *
         * @return Zwracany jest przygotowany przez nas samochod
         */
        public Samochod buduj() {
            return new Samochod(this);
        }

    }

    /**
     * Metoda przygotowujaca nam tekstowe reprezentacje naszego obiektu samochod.
     *
     * @return Zwracana jest tekstowa postac naszego obiektu
     */
    @Override
    public String toString() {
        List<String> dodatkoweInformacjeLista = budowaListyInformacji();
        return String.format("Sprzedam samochod %s %s.\nInformacje o samochodzie: %s",
                marka, model, dodatkoweInformacjeLista);
    }

    /**
     * Metoda budujaca liste elementow zawierajacych informacje o samochodzie
     *
     * @return Zwraca liste zawierajaca informacje dodatkowe o samochodzie
     */
    private List<String> budowaListyInformacji() {
        List<String> dodatkoweInformacje = new ArrayList<>();
        dodatkoweInformacje.add(String.format("Cena: %d zł", cena));
        if (stan != null) {
            dodatkoweInformacje.add(String.format("Stan: %s", stan));
        }
        if (rokProdukcji != null) {
            dodatkoweInformacje.add(String.format("Rok produkcji: %d", rokProdukcji));
        }
        if (pojemnoscSilnika != null) {
            dodatkoweInformacje.add(String.format("Pojemnosc silnika: %d cm3", pojemnoscSilnika));
        }
        if (przebieg != null) {
            dodatkoweInformacje.add(String.format("Przebieg: %d km", przebieg));
        }
        if (rodzajPaliwa != null) {
            dodatkoweInformacje.add(String.format("Rodzaj paliwa: %s", rodzajPaliwa));
        }
        return dodatkoweInformacje;
    }
}

