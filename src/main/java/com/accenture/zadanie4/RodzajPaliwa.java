package com.accenture.zadanie4;

/**
 * Klasa enum majaca ustalic rodzaj paliwa dla samochodu.
 */
public enum RodzajPaliwa {
    BENZYNA, DISEL, ELEKTRYCZNY, HYBRYDOWY, LPG
}
