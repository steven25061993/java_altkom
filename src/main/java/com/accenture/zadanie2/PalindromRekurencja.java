package com.accenture.zadanie2;

/**
 * Klasa, sprawdzająca czy podany przez uzytkownika wyraz jest palindromem.Robi to za pomoca mechanizmu rekurencji.
 *
 * @author Damian Futyma
 */
public final class PalindromRekurencja {

    private PalindromRekurencja() {
    }

    /**
     * Metoda za pomoca mechanizmu rekurencji bada czy podany tekst jest palindromem.
     *
     * @param tekst Podany przez uzytkownika ciag znakow typu String
     * @return Metoda zwraca true gdy podany wyraz jest palindromem, false gdy nie jest oraz null gdy tekst jest pusty lub rowny null
     */
    public static Boolean czyPalindrom(final String tekst) {
        if (czyTekstJestPustyLubNull(tekst)) {
            return null;
        }

        if (czyPierwszyIOstatniZnakSaRowne(tekst)) {
            return czyPierwszyIOstatniZnakSaRowne(zmniejszTekstOSkrajneZnaki(tekst));
        }
        return false;
    }

    /**
     * Metoda wewnatrz klasy czyPalindrom sprawdzajaca czy podany przez uzytkownika tekst ma dlugosc rowna 0 lub
     * czy jej wartosc to null
     *
     * @param tekst Podany przez uzytkownika ciag znakow typu String
     * @return wartosc true dla ciagu znakow o dlugosci 0 lub dla wartosci null
     */
    private static boolean czyTekstJestPustyLubNull(final String tekst) {
        if (tekst == null || tekst.length() == 0) {
            return true;
        }
        return false;
    }

    /**
     * Metoda ma na celu skrocenie tekstu podanego przez dwa skrajne znaki
     *
     * @param tekst Podany przez uzytkownika ciag znakow typu String
     * @return Tekst podany przez uzytkownika skrocony o dwa skrajne znaki
     */
    private static String zmniejszTekstOSkrajneZnaki(final String tekst) {
        return tekst.substring(1, tekst.length() - 1);
    }

    /**
     * Metoda sprawdza czy pierwszy i ostatni znak w tekscie sa takie same.
     *
     * @param tekst Podany przez uzytkownika ciag znakow typu String
     * @return Wartosc true dla takich samych znakow, wartosci false dla znakow roznych.
     */
    private static boolean czyPierwszyIOstatniZnakSaRowne(final String tekst) {
        return tekst.charAt(0) == tekst.charAt(tekst.length() - 1);
    }

}
