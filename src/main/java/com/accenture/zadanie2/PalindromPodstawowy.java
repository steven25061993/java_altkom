package com.accenture.zadanie2;

/**
 * Klasa sprawdzajaca czy podany tekst jest palindromem z uzyciem podstawowej petli for.
 */
public final class PalindromPodstawowy {
    /**
     * Stala o wartosci 2 sluzaca do podzielenia tablicy reprezentujace tekst podany przez uzytkowanika na pol
     *
     * @author Damian Futyma
     */
    private static final int PODZIEL_NA_POL = 2;

    private PalindromPodstawowy() {
    }

    /**
     * Metoda sprawdza na poczatku czy tekst nie jest pusty. Następnie dzieli tekst na tablice char i w petli porownuje ze soba
     * elementy wg.klucz pierwszy z ostatnim, drugi z przedostatnim itp.
     * Metoda wylapuje NullPointerException gdy do zmiennej tekst przypisany jest null.
     *
     * @param tekst Podany przez uzytkownika ciag znakow typu String
     * @return Metoda zwraca true gdy podany wyraz jest palindromem, false gdy nie jest oraz null gdy tekst jest pusty lub rowny null.
     */
    public static Boolean czyPalindrom(final String tekst) {
        try {
            if (tekst.length() == 0) {
                return null;
            } else {
                char[] podzielonyTekst = tekst.toCharArray();
                for (int i = 0; i < podzielonyTekst.length / PODZIEL_NA_POL; i++) {
                    if (podzielonyTekst[i] != podzielonyTekst[podzielonyTekst.length - i - 1]) {
                        return false;
                    }
                }
            }
            return true;
        } catch (NullPointerException e) {
            return null;
        }

    }
}
