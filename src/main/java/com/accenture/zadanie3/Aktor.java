package com.accenture.zadanie3;

/**
 * Klasa aktor, majaca pomoc w tworzeniu instancji aktora
 */
public class Aktor {
    /**
     * String zawierajacy imie i nazwisko aktora podzielone spacja
     */
    private String tozsamosc;
    /**
     * Plec z klasy enum plec
     */
    private Plec plec;

    /**
     * Konstruktor klasy Aktor
     * @param tozsamosc String bedacy reprezentacja imienia wraz z nazwiskiem aktora
     * @param plec      Enum bedacy reprezentacja plci aktora
     */
    public Aktor(final String tozsamosc, final Plec plec) {
        this.tozsamosc = tozsamosc;
        this.plec = plec;
    }

    /**
     * @return Plec instancji aktora z klasy enum
     */
    public Plec pobierzPlec() {
        return plec;
    }

    /**
     * @return Imie i nazwisko aktora podzielone spacja
     */
    public String pobierzTozsamosc() {
        return tozsamosc;
    }

    /**
     * Zostala tutaj uzyta oryginalna, angielska nazwa 'toString' aby wartosc zostala rozponana przez kompilator.
     * Jest to jedyne odstepstwo od przyjetego w kodzie jezyka polskiego
     *
     * @return Zwraca reprezentacje aktora w postaci String w formiecie 'tozsamosc (plec)'.
     */
    @Override
    public String toString() {
        return String.format("%s(%s)", pobierzTozsamosc(), pobierzPlec());
    }


}
