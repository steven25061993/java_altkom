package com.accenture.zadanie3;

/**
 * Klasa enum zawierajaca dwie reprezencje plci: K dla kobiet i M dla mezczyzn.
 */
public enum Plec {
    K,M
}
