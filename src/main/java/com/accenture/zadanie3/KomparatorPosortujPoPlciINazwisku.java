package com.accenture.zadanie3;

import java.util.Comparator;

/**
 * Komparator pozwalajacy na posortowanie listy aktorow. Pierwszym kryteriu porowniania jest plec natomiast
 * drugim nazwisko z aktora.
 */
public class KomparatorPosortujPoPlciINazwisku implements Comparator<Aktor> {
    /**
     * @param o1 obiekt z klasy Aktor
     * @param o2 obiekt z klasy Aktor
     * @return Obiekty klasy Aktor posortowane we wlasciwej kolejnosci, wzgledem plci a nastepnie wzgledem nazwiska
     */
    @Override
    public int compare(final Aktor o1, final Aktor o2) {
        int porownywaniePlci = o1.pobierzPlec().compareTo(o2.pobierzPlec());
        if (porownywaniePlci != 0) {
            return porownywaniePlci;
        }
        return o1.pobierzTozsamosc().split(" ")[1].compareTo(o2.pobierzTozsamosc().split(" ")[1]);
    }
}
